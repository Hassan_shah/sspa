package com.app.sspa.retrofit;


import com.app.sspa.entities.GeneralAttributeEnt;
import com.app.sspa.entities.TeacherProifleEnt;
import com.app.sspa.entities.announcementEnt;
import com.app.sspa.entities.ResponseWrapper;
import com.app.sspa.entities.StudentEnt;
import com.app.sspa.entities.StudentProifleEnt;
import com.app.sspa.entities.teacherEnt;

import com.app.sspa.entities.attributeent;
import com.app.sspa.entities.classent;
import com.app.sspa.entities.performanceEnt;
import com.app.sspa.entities.sectionent;
import com.app.sspa.entities.userent;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface WebService {
    @FormUrlEncoded
    @POST("login.php")
    Call<ResponseWrapper<userent>> makeLogin(@Field("username") String username,
                                             @Field("password") String password,
                                             @Field("who") Integer level);

    @FormUrlEncoded
    @POST("registerstudent.php")
    Call<ResponseWrapper<String>> registerStudent(@Field("fname") String fname,
                                                  @Field("lname") String lname,
                                                  @Field("gender") String gender,
                                                  @Field("age") String age,
                                                  @Field("address") String address,
                                                  @Field("pname") String pname,
                                                  @Field("class") Integer classes,
                                                  @Field("section") Integer section,
                                                  @Field("password") String password);

    @FormUrlEncoded
    @POST("registerteacher.php")
    Call<ResponseWrapper<String>> registerTeacher(@Field("fname") String fname,
                                                  @Field("gender") String gender,
                                                  @Field("age") String age,
                                                  @Field("qualification") String qualification,
                                                  @Field("address") String address,
                                                  @Field("class") Integer classes,
                                                  @Field("section") Integer section,
                                                  @Field("password") String password);

    @FormUrlEncoded
    @POST("addsubject.php")
    Call<ResponseWrapper<String>> addsubject(@Field("scode") String scode, @Field("sname") String sname);

    @FormUrlEncoded
    @POST("addattribute.php")
    Call<ResponseWrapper<String>> addattribute(@Field("attname") String attname);

    @FormUrlEncoded
    @POST("addannouncement.php")
    Call<ResponseWrapper<String>> addannouncement(@Field("announcecode") String announcecode,
                                                  @Field("title") String title,
                                                  @Field("description") String description);

    @GET("getsections.php")
    Call<ResponseWrapper<ArrayList<sectionent>>> getsections();

    @GET("getteacher.php")
    Call<ResponseWrapper<ArrayList<teacherEnt>>> getteacher();

    @GET("getstudent.php")
    Call<ResponseWrapper<ArrayList<StudentEnt>>> getstudent();

    @GET("getclass.php")
    Call<ResponseWrapper<ArrayList<classent>>> getclass();

    @GET("getattribute.php")
    Call<ResponseWrapper<ArrayList<attributeent>>> getattribute();

    @GET("getannouncement.php")
    Call<ResponseWrapper<ArrayList<announcementEnt>>> getannouncement();

    @GET("getattribute.php")
    Call<ResponseWrapper<ArrayList<GeneralAttributeEnt>>> getGeneralAttributes();


    @FormUrlEncoded
    @POST("getperformance.php")
    Call<ResponseWrapper<ArrayList<performanceEnt>>> getperformance(@Field("studentid") String studentid,
                                                                    @Field("year") String year,
                                                                    @Field("term") String term);

    @FormUrlEncoded
    @POST("addmarks.php")
    Call<ResponseWrapper> addAcademicMarks(@Field("student_id") String studentid,
                                           @Field("year") String year,
                                           @Field("term") String term,
                                           @Field("english") String english,
                                           @Field("science") String science,
                                           @Field("Mathmatics") String Mathmatics,
                                           @Field("urdu") String urdu,
                                           @Field("islamiat") String islamiat,
                                           @Field("social_studies") String social_studies,
                                           @Field("Computer") String Computer,
                                           @Field("subcode") String subcode,
                                           @Field("test") String test,
                                           @Field("exam") String exam,
                                           @Field("tname") String tname
    );

    @FormUrlEncoded
    @POST("addgeneralattributemarks.php")
    Call<ResponseWrapper> addGeneralMarks(@Field("student_id") String studentid,
                                          @Field("year") String year,
                                          @Field("term") String term,
                                          @Field("cleanliness") String cleanliness,
                                          @Field("general_behavior") String general_behavior,
                                          @Field("class_work") String class_work,
                                          @Field("home_work") String home_work,
                                          @Field("attendance") String attendance,
                                          @Field("extra_activities") String extra_activities
                                          );


    @FormUrlEncoded
    @POST("getstudentbyid.php")
    Call<ResponseWrapper<ArrayList<StudentProifleEnt>>> getstudentProfile(@Field("studentid") String studentid);

    @FormUrlEncoded
    @POST("getteacherbyid.php")
    Call<ResponseWrapper<ArrayList<TeacherProifleEnt>>> getTeacherProfile(@Field("teacherid") String teacherid);
}