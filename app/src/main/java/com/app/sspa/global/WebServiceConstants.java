package com.app.sspa.global;

public class WebServiceConstants {
    public static final String SERVICE_URL = "http://mp3juice.io/school/api/";
    public static final Integer SUCCESS_RESPONSE_CODE = 9000;
    public static final String makeLogin = "makeLogin";
    public static final String registerStudent = "registerStudent";
    public static final String registerTeacher = "registerTeacher";
    public static final String addsubject = "addsubject";
    public static final String addattribute = "addattribute";
    public static final String addannouncement = "addannouncement";
    public static final String getsections = "getsections";
    public static final String getteacher = "getteacher";
    public static final String getstudent = "getstudent";
    public static final String academicmarks = "academicmarks";
    public static final String getclass = "getclass";
    public static final String getattribute = "getattribute";
    public static final String getannouncement = "getannouncement";
    public static final String getperformance = "getperformance";
    public static final String getGeneralAttribute = "getGeneralAttribute";
    public static final String GeneralMarks = "GeneralMarks";
}
