package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AdminOptionsFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.txt_brandName)
    TextView txtBrandName;
    @BindView(R.id.btn_add_subject)
    TextView btnAddSubject;
    @BindView(R.id.btn_add_announcement)
    TextView btnAddAnnouncement;
    @BindView(R.id.btn_add_attribute)
    TextView btnAddAttribute;
    @BindView(R.id.btn_add_section)
    TextView btnAddSection;

    public static AdminOptionsFragment newInstance() {
        return new AdminOptionsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_admin_option, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Administrations");
    }

    @OnClick({R.id.btn_add_subject, R.id.btn_add_announcement, R.id.btn_add_attribute, R.id.btn_add_section,R.id.btn_add_Class})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_add_subject:
                getDockActivity().replaceDockableFragment(AdminSubjectFragment.newInstance(), "AdminSubjectFragment");
                break;
            case R.id.btn_add_announcement:
                getDockActivity().replaceDockableFragment(AdminAnnouncementFragment.newInstance(), "AdminAnnouncementFragment");
                break;
            case R.id.btn_add_attribute:
                getDockActivity().replaceDockableFragment(AdminAttributeFragment.newInstance(), "AdminAttributeFragment");
                break;
            case R.id.btn_add_section:
                getDockActivity().replaceDockableFragment(AddSectionFragment.newInstance(), "AddSectionFragment");
                break;
            case R.id.btn_add_Class:
                getDockActivity().replaceDockableFragment(AddClassFragment.newInstance(), "AddClassFragment");
                break;

        }
    }
}