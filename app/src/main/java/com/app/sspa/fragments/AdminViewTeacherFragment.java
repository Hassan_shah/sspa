package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.teacherEnt;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.ui.adapters.ArrayListAdapter;
import com.app.sspa.ui.viewbinders.abstracts.TeachersBinder;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AdminViewTeacherFragment extends BaseFragment {
    @BindView(R.id.txt_nodata)
    TextView txtNodata;
    @BindView(R.id.lv_teachers)
    ListView lvTeachers;
    Unbinder unbinder;
    private ArrayListAdapter<teacherEnt> adapter;
    private ArrayList<teacherEnt> collections;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ArrayListAdapter<teacherEnt>(getDockActivity(), new TeachersBinder());
    }

    public static AdminViewTeacherFragment newInstance() {
        return new AdminViewTeacherFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_view_teachers, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        serviceHelper.enqueueCall(webService.getteacher(), WebServiceConstants.getteacher);
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.getteacher:
                setData((ArrayList<teacherEnt>) result);
                break;
        }
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("View Teachers");
    }

    private void setData(ArrayList<teacherEnt> result) {
        collections = new ArrayList<>();
       collections.addAll(result);
        if (collections.size() <= 0) {
            lvTeachers.setVisibility(View.GONE);
            txtNodata.setVisibility(View.VISIBLE);
        } else {
            lvTeachers.setVisibility(View.VISIBLE);
            txtNodata.setVisibility(View.GONE);
        }
        adapter.clearList();
        adapter.addAll(collections);
        lvTeachers.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}