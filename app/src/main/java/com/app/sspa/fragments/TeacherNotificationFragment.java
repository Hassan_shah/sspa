package com.app.sspa.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Saeed Hyder on 12/1/2017.
 */
public class TeacherNotificationFragment extends BaseFragment {

    @BindView(R.id.viewNotification)
    TextView viewNotification;
    @BindView(R.id.sendNotification)
    TextView sendNotification;
    Unbinder unbinder;

    public static TeacherNotificationFragment newInstance() {
        return new TeacherNotificationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teacher_notification, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Teacher Notification");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.viewNotification, R.id.sendNotification})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.viewNotification:
                getDockActivity().replaceDockableFragment(TeacherViewNotificationFragment.newInstance(),"TeacherViewNotificationFragment");
                break;
            case R.id.sendNotification:
                getDockActivity().replaceDockableFragment(AdminAnnouncementFragment.newInstance(),"AdminAnnouncementFragment");
                break;
        }
    }
}
