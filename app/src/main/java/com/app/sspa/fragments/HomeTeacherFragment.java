package com.app.sspa.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.app.sspa.R;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.ui.views.TitleBar;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Saeed Hyder on 11/28/2017.
 */
public class HomeTeacherFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.imageSlider)
    SliderLayout imageSlider;
    /*@BindView(R.id.pagerIndicator)
    PagerIndicator pagerIndicator;*/
    @BindView(R.id.rl_notification)
    LinearLayout rlNotification;
    @BindView(R.id.rl_profile)
    LinearLayout rlProfile;
    @BindView(R.id.rl_viewMarks)
    LinearLayout rlViewMarks;
    @BindView(R.id.img_favourite_badge)
    RelativeLayout imgFavouriteBadge;
    @BindView(R.id.rl_addMarks)
    LinearLayout rlAddMarks;

    public static HomeTeacherFragment newInstance() {
        return new HomeTeacherFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_teacher, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setImageGallery();
        getMainActivity().refreshTeacherSideMenu();

    }

    private void setImageGallery() {

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(0, R.drawable.bg0);
        arrayList.add(1, R.drawable.bg5);
        arrayList.add(2, R.drawable.bg2);
        arrayList.add(3, R.drawable.bg3);
        arrayList.add(4, R.drawable.bg4);
        arrayList.add(5, R.drawable.bg6);

        for (int i = 0; i < arrayList.size(); i++) {
            TextSliderView textSliderView = new TextSliderView(getDockActivity());
            textSliderView
                    .description("")
                    .image(arrayList.get(i));
            imageSlider.addSlider(textSliderView);
        }

        /*for (Integer image : arrayList) {
            DefaultSliderView textSliderView = new DefaultSliderView(getDockActivity());
            // initialize a SliderLayout
            textSliderView
                    .image(image)
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {

                        }
                    });

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", image + "");

            imageSlider.addSlider(textSliderView);
        }


        imageSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        imageSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        imageSlider.setCustomAnimation(new DescriptionAnimation());
        imageSlider.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        imageSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        imageSlider.stopAutoCycle();*/
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showMenuButton();
        titleBar.setSubHeading(getString(R.string.teacher_home));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.rl_profile, R.id.rl_notification, R.id.rl_viewMarks, R.id.rl_addMarks})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_profile:
                getDockActivity().replaceDockableFragment(TeacherProfileFragment.newInstance(), "TeacherProfileFragment");
                break;
            case R.id.rl_notification:
                getDockActivity().replaceDockableFragment(TeacherNotificationFragment.newInstance(), "TeacherNotificationFragment");
               // getDockActivity().replaceDockableFragment(TeacherViewNotificationFragment.newInstance(), "TeacherViewNotificationFragment");
                break;
            case R.id.rl_viewMarks:
                getDockActivity().replaceDockableFragment(TeacherViewMarksFragment.newInstance(), "TeacherViewMarksFragment");
                break;
            case R.id.rl_addMarks:
                getDockActivity().replaceDockableFragment(TeacherAddMarks.newInstance(), "TeacherAddMarks");
                break;
        }
    }
}
