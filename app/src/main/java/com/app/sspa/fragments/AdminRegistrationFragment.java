package com.app.sspa.fragments;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.classent;
import com.app.sspa.entities.sectionent;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.helpers.UIHelper;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Hasssan on 11/28/2017.
 */

public class AdminRegistrationFragment extends BaseFragment {


    @BindView(R.id.txt_brandName)
    TextView txtBrandName;
    @BindView(R.id.user_role)
    Spinner userRole;
    @BindView(R.id.user_name)
    EditText userName;
    @BindView(R.id.user_gender)
    Spinner userGender;
    @BindView(R.id.user_age)
    EditText userAge;
    @BindView(R.id.user_qualification)
    EditText userQualification;
    @BindView(R.id.con_qualification)
    TextInputLayout conQualification;
    @BindView(R.id.user_parentname)
    EditText userParentname;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.con_parent)
    TextInputLayout conParent;
    @BindView(R.id.user_address)
    EditText userAddress;
    @BindView(R.id.user_class)
    Spinner userClass;
    @BindView(R.id.user_section)
    Spinner userSection;
    @BindView(R.id.register_btn)
    Button registerBtn;
    @BindView(R.id.cancel_btn)
    Button cancelBtn;

    public static AdminRegistrationFragment newInstance() {
        return new AdminRegistrationFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_admin_registration, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        //addEmptyStringValidator((AnyEditTextView) userName, (AnyEditTextView) userAddress, (AnyEditTextView) userAge, (AnyEditTextView) userParentname,
                //(AnyEditTextView) userQualification);
        setRoleSpinner();
        setClassSpinner();
        setGenderSpinner();
        setSectionSpinner();


    }

    private void setRoleSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getDockActivity(), R.array.role_registration, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userRole.setAdapter(adapter);
        userRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    conQualification.setVisibility(View.GONE);
                    conParent.setVisibility(View.VISIBLE);
                } else {
                    conQualification.setVisibility(View.VISIBLE);
                    conParent.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Registrations");
    }

    ArrayList<sectionent> sectionents;
    ArrayList<classent> classents;

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.getsections:
                ArrayList<String> sections = new ArrayList<String>();
                sectionents = (ArrayList<sectionent>) result;
                for (sectionent ent : sectionents
                        ) {
                    sections.add(ent.getSection_name());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, sections);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                userSection.setAdapter(adapter);
                break;
            case WebServiceConstants.getclass:
                ArrayList<String> classes = new ArrayList<String>();
                classents = (ArrayList<classent>) result;
                for (classent ent : classents
                        ) {
                    classes.add(ent.getClass_name());
                }
                ArrayAdapter<String> adapterclass = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, classes);
                adapterclass.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                userClass.setAdapter(adapterclass);
                break;
            case WebServiceConstants.registerStudent:
                UIHelper.showShortToastInCenter(getDockActivity(), "Registration Successfull");
                getDockActivity().popFragment();
                break;
            case WebServiceConstants.registerTeacher:
                UIHelper.showShortToastInCenter(getDockActivity(), "Registration Successfull");
                getDockActivity().popFragment();
                break;
        }
    }

    private void setSectionSpinner() {
        serviceHelper.enqueueCall(webService.getsections(), WebServiceConstants.getsections);


    }

    private void setGenderSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getDockActivity(), R.array.gender, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userGender.setAdapter(adapter);

    }


    private void setClassSpinner() {
        serviceHelper.enqueueCall(webService.getclass(), WebServiceConstants.getclass);

    }


    @OnClick({R.id.register_btn, R.id.cancel_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.register_btn:
                if (userRole.getSelectedItemPosition() == 0) {
                    serviceHelper.enqueueCall(webService.registerStudent(userName.getText().toString(),
                            userName.getText().toString(),
                            (String) userGender.getSelectedItem(),
                            userAge.getText().toString(),
                            userAddress.getText().toString(),
                            userParentname.getText().toString(),
                            Integer.parseInt(classents.get(userClass.getSelectedItemPosition()).getClass_id()),
                            Integer.parseInt(sectionents.get(userSection.getSelectedItemPosition()).getSection_id()),
                            password.getText().toString()), WebServiceConstants.registerStudent);
                } else {
                    serviceHelper.enqueueCall(webService.registerTeacher(userName.getText().toString(),
                            (String) userGender.getSelectedItem(),
                            userAge.getText().toString(),
                            userQualification.getText().toString(),
                            userAddress.getText().toString(),
                            Integer.parseInt(classents.get(userClass.getSelectedItemPosition()).getClass_id()),
                            Integer.parseInt(sectionents.get(userSection.getSelectedItemPosition()).getSection_id()),
                            password.getText().toString()), WebServiceConstants.registerStudent);
                }
                break;
            case R.id.cancel_btn:
                getDockActivity().popFragment();
                break;
        }
    }


}
