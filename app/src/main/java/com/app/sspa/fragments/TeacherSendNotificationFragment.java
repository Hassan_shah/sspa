package com.app.sspa.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.app.sspa.R;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Saeed Hyder on 12/1/2017.
 */
public class TeacherSendNotificationFragment extends BaseFragment {

    @BindView(R.id.mainSpinner)
    Spinner mainSpinner;
    @BindView(R.id.secondSpinner)
    Spinner secondSpinner;
    @BindView(R.id.message)
    EditText message;
    @BindView(R.id.send)
    Button send;
    Unbinder unbinder;

    public static TeacherSendNotificationFragment newInstance() {
        return new TeacherSendNotificationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teacher_send_notification, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

      spinners();
    }


    private void spinners() {

        List<String> categories = new ArrayList<String>();
        categories.add("Student");
        categories.add("Class");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mainSpinner.setAdapter(dataAdapter);

        List<String> students = new ArrayList<String>();
        students.add("Student1");
        students.add("Student2");
        students.add("Student3");
        students.add("Student4");


        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, students);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        secondSpinner.setAdapter(dataAdapter1);

    }



    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Send Notification");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.send)
    public void onClick() {
        getDockActivity().replaceDockableFragment(HomeTeacherFragment.newInstance(),"HomeTeacherFragment");
    }
}
