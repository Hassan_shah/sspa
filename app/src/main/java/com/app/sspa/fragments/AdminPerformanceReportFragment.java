package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.StudentEnt;
import com.app.sspa.entities.performanceEnt;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.helpers.UIHelper;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AdminPerformanceReportFragment extends BaseFragment {
    @BindView(R.id.txt_brandName)
    TextView txtBrandName;
    @BindView(R.id.student_name)
    Spinner studentName;
    @BindView(R.id.year)
    Spinner year;
    @BindView(R.id.exam_type)
    Spinner examType;
    @BindView(R.id.add_btn)
    Button addBtn;
    @BindView(R.id.lv_performance)
    ListView lvPerformance;
    Unbinder unbinder;

    public static AdminPerformanceReportFragment newInstance() {
        return new AdminPerformanceReportFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_admin_performance_report, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        setspinners();
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceHelper.enqueueCall(webService.getperformance(studentEnts.get(studentName.getSelectedItemPosition()).getId(),
                        (String) year.getSelectedItem(),(String)examType.getSelectedItem()),WebServiceConstants.getperformance);
            }
        });

    }

    private void setspinners() {
        //studentSpinner
        serviceHelper.enqueueCall(webService.getstudent(), WebServiceConstants.getstudent);
        //Year Spinner
        String[] yeararrar = {"2014",
                "2015",
                "2016",
                "2017"};
        ArrayAdapter<CharSequence> year_adapter = ArrayAdapter.createFromResource(getDockActivity(), R.array.year, android.R.layout.simple_spinner_item);
        year_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        year.setAdapter(year_adapter);
        //Exam Type Spinner
        ArrayAdapter<CharSequence> exam_adapter = ArrayAdapter.createFromResource(getDockActivity(), R.array.exam, android.R.layout.simple_spinner_item);
        exam_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        examType.setAdapter(exam_adapter);
    }

    ArrayList<StudentEnt> studentEnts;

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("View Performance");
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.getstudent:
                studentEnts = ((ArrayList<StudentEnt>) result);
                ArrayList<String> studentNames = new ArrayList<String>();
                for (StudentEnt ent : studentEnts
                        ) {
                    studentNames.add(ent.getName());
                }
                ArrayAdapter<String> student_adapter = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, studentNames);
                student_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                studentName.setAdapter(student_adapter);
                break;
            case WebServiceConstants.getperformance:
                ArrayList<performanceEnt>ent = (ArrayList<performanceEnt>)result;
                if (ent!=null && ent.size() > 0)
                    getDockActivity().replaceDockableFragment(ReportCardFragment.newInstance(ent,(String) year.getSelectedItem(),(String)examType.getSelectedItem()),"ReportCardFragment");
                else
                    UIHelper.showShortToastInCenter(getDockActivity(), "No Data To Show");

                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}