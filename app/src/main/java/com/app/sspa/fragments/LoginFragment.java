package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.userent;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.helpers.UIHelper;
import com.app.sspa.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginFragment extends BaseFragment {


    @BindView(R.id.txt_brandName)
    TextView txtBrandName;
    @BindView(R.id.user_gender)
    Spinner userGender;
    @BindView(R.id.user_name)
    EditText userName;
    @BindView(R.id.user_password)
    EditText userPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.scrollview_bigdaddy)
    LinearLayout scrollviewBigdaddy;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);


    }


    @Override
    public void setTitleBar(TitleBar titleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar);
        titleBar.hideTitleBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        setGenderSpinner();
        return view;

    }


    private void setGenderSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getDockActivity(), R.array.role, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userGender.setAdapter(adapter);
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.makeLogin:
                userent ent = (userent) result;
                prefHelper.setLoginStatus(true);
                if (ent.getLEVEL().equals("1")) {
                    prefHelper.setIsAdmin(true);
                    prefHelper.setAdminId(Integer.parseInt(ent.getId()));
                    getDockActivity().replaceDockableFragment(AdminHomeFragment.newInstance(), "AdminHomeFragment");
                } else if (ent.getLEVEL().equals("2")) {
                    prefHelper.setIsTeacher(true);
                    prefHelper.setTeacherId(Integer.parseInt(ent.getId()));
                    getDockActivity().replaceDockableFragment(HomeTeacherFragment.newInstance(), "HomeTeacherFragment");
                } else {
                    prefHelper.setIsStudent(true);
                    prefHelper.setStudentId(Integer.parseInt(ent.getId()));
                    prefHelper.setStudentName(ent.getFname());
                    getDockActivity().replaceDockableFragment(HomeStudentFragment.newInstance(), "HomeStudentFragment");
                }
                break;
        }
    }

    @OnClick(R.id.btn_login)
    public void onViewClicked() {
        if (isvalidated()) {

            if (userGender.getSelectedItemPosition() == 1) {
                serviceHelper.enqueueCall(webService.makeLogin(userName.getText().toString(), userPassword.getText().toString(),
                        userGender.getSelectedItemPosition()), WebServiceConstants.makeLogin);
            } else if (userGender.getSelectedItemPosition() == 2) {
                serviceHelper.enqueueCall(webService.makeLogin(userName.getText().toString(), userPassword.getText().toString(),
                        userGender.getSelectedItemPosition()), WebServiceConstants.makeLogin);
            } else {
                serviceHelper.enqueueCall(webService.makeLogin(userName.getText().toString(), userPassword.getText().toString(),
                        5), WebServiceConstants.makeLogin);
            }
        }
    }

    private boolean isvalidated() {
        if (userName.getText().toString().equals("")) {
            userName.setError(getString(R.string.name_empty_error));
            if (userName.requestFocus()) {
                getDockActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return false;
        } else if (userPassword.getText().toString().equals("")) {
            userPassword.setError(getString(R.string.password_empty_error));
            if (userPassword.requestFocus()) {
                getDockActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return false;
        } else if (userPassword.getText().toString().length() < 4) {
            userPassword.setError(getString(R.string.password_short_error));
            if (userPassword.requestFocus()) {
                getDockActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return false;
        } else if (userGender.getSelectedItemPosition() == 0) {
            UIHelper.showShortToastInCenter(getDockActivity(), getString(R.string.gender_error));
            return false;
        } else {
            return true;
        }
    }


}
