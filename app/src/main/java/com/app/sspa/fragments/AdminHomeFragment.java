package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.ui.views.TitleBar;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Hasssan on 11/28/2017.
 */

public class AdminHomeFragment extends BaseFragment {


    @BindView(R.id.slider)
    SliderLayout slider;
    @BindView(R.id.btn_registrations)
    TextView btnRegistrations;
    @BindView(R.id.btn_administration)
    TextView btnAdministration;
    @BindView(R.id.btn_records)
    TextView btnRecords;
    @BindView(R.id.btn_performance)
    TextView btnPerformance;
    @BindView(R.id.rl_registrations)
    LinearLayout rlRegistrations;
    @BindView(R.id.rl_administration)
    LinearLayout rlAdministration;
    @BindView(R.id.rl_records)
    LinearLayout rlRecords;
    @BindView(R.id.rl_performance)
    LinearLayout rlPerformance;

    public static AdminHomeFragment newInstance() {
        return new AdminHomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_admin_home, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        setSlider();
        getMainActivity().refreshAdminSideMenu();

    }


    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showMenuButton();
        titleBar.setSubHeading("Home");
    }

    private void setSlider() {
        int[] sliderbg = {
                R.drawable.bg0,
                R.drawable.school,
                R.drawable.bg2,
                R.drawable.bg3,
                R.drawable.bg4,
                R.drawable.bg5,
                R.drawable.bg6};
        for (int i = 0; i < sliderbg.length; i++) {
            TextSliderView textSliderView = new TextSliderView(getDockActivity());
            textSliderView
                    .description("")
                    .image(sliderbg[i]);
            slider.addSlider(textSliderView);
        }


    }


    @OnClick({R.id.btn_registrations, R.id.btn_administration, R.id.btn_records, R.id.btn_performance})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_registrations:
                getDockActivity().replaceDockableFragment(AdminRegistrationFragment.newInstance(), "AdminRegistrationFragment");
                break;
            case R.id.btn_administration:
                getDockActivity().replaceDockableFragment(AdminOptionsFragment.newInstance(), "AdminOptionsFragment");
                break;
            case R.id.btn_records:
                getDockActivity().replaceDockableFragment(AdminViewRecordFragment.newInstance(), "AdminViewRecordFragment");
                break;
            case R.id.btn_performance:
                getDockActivity().replaceDockableFragment(AdminPerformanceReportFragment.newInstance(), "AdminPerformanceReportFragment");
                break;
        }
    }



    @OnClick({R.id.rl_registrations, R.id.rl_administration, R.id.rl_records, R.id.rl_performance})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_registrations:
                getDockActivity().replaceDockableFragment(AdminRegistrationFragment.newInstance(), "AdminRegistrationFragment");
                break;
            case R.id.rl_administration:
                getDockActivity().replaceDockableFragment(AdminOptionsFragment.newInstance(), "AdminOptionsFragment");
                break;
            case R.id.rl_records:
                getDockActivity().replaceDockableFragment(AdminViewRecordFragment.newInstance(), "AdminViewRecordFragment");
                break;
            case R.id.rl_performance:
                getDockActivity().replaceDockableFragment(AdminPerformanceReportFragment.newInstance(), "AdminPerformanceReportFragment");
                break;
        }
    }
}
