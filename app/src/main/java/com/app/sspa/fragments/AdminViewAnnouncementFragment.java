package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.announcementEnt;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.ui.adapters.ArrayListAdapter;
import com.app.sspa.ui.viewbinders.abstracts.AnnouncementBinder;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AdminViewAnnouncementFragment extends BaseFragment {
    @BindView(R.id.txt_nodata)
    TextView txtNodata;
    @BindView(R.id.lv_announcement)
    ListView lvAnnouncement;
    Unbinder unbinder;
    private ArrayListAdapter<announcementEnt> adapter;
    private ArrayList<announcementEnt> collections;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ArrayListAdapter<announcementEnt>(getDockActivity(), new AnnouncementBinder());
    }

    public static AdminViewAnnouncementFragment newInstance() {
        return new AdminViewAnnouncementFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_view_announcement, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    private void setData(ArrayList<announcementEnt>result) {
        collections = new ArrayList<>();
       collections.addAll(result);

        if (collections.size() <= 0) {
            lvAnnouncement.setVisibility(View.GONE);
            txtNodata.setVisibility(View.VISIBLE);
        } else {
            lvAnnouncement.setVisibility(View.VISIBLE);
            txtNodata.setVisibility(View.GONE);
        }
        adapter.clearList();
        adapter.addAll(collections);
        lvAnnouncement.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        serviceHelper.enqueueCall(webService.getannouncement(), WebServiceConstants.getannouncement);
    }
    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.getannouncement:
                setData((ArrayList<announcementEnt>) result);
                break;
        }
    }
    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("View Announcements");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}