package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.StudentEnt;
import com.app.sspa.entities.userent;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.ui.adapters.ArrayListAdapter;
import com.app.sspa.ui.viewbinders.abstracts.StudentBinder;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AdminViewStudentFragment extends BaseFragment {
    @BindView(R.id.txt_nodata)
    TextView txtNodata;
    @BindView(R.id.lv_students)
    ListView lvStudents;
    Unbinder unbinder;
    private ArrayListAdapter<StudentEnt> adapter;
    private ArrayList<StudentEnt> collections;

    public static AdminViewStudentFragment newInstance() {
        return new AdminViewStudentFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ArrayListAdapter<StudentEnt>(getDockActivity(),new StudentBinder());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_view_student, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        serviceHelper.enqueueCall(webService.getstudent(), WebServiceConstants.getstudent);
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag){
            case WebServiceConstants.getstudent:
                setData((ArrayList<StudentEnt>)result);
                break;
        }
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("View Students");
    }

    private void setData(ArrayList<StudentEnt> datacollections) {
        collections = new ArrayList<>();
        collections.addAll(datacollections);
        if (collections.size()<=0){
            lvStudents.setVisibility(View.GONE);
            txtNodata.setVisibility(View.VISIBLE);
        }
        else {
            lvStudents.setVisibility(View.VISIBLE);
            txtNodata.setVisibility(View.GONE);
        }
        adapter.clearList();
        adapter.addAll(collections);
        lvStudents.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}