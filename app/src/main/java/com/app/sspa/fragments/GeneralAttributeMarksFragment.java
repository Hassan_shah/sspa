package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.app.sspa.R;
import com.app.sspa.entities.GeneralAttributeEnt;
import com.app.sspa.entities.StudentEnt;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.helpers.UIHelper;
import com.app.sspa.ui.adapters.ArrayListAdapter;
import com.app.sspa.ui.viewbinders.abstracts.GeneralAttItemBinder;
import com.app.sspa.ui.views.ExpandedListView;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by saeedhyder on 12/12/2017.
 */
public class GeneralAttributeMarksFragment extends BaseFragment {
    @BindView(R.id.lv_generalAtt)
    ExpandedListView lvGeneralAtt;
    Unbinder unbinder;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.studentName)
    Spinner studentName;
    @BindView(R.id.year)
    Spinner year;
    @BindView(R.id.term)
    Spinner term;
    @BindView(R.id.mainframe)
    LinearLayout mainframe;

    private ArrayListAdapter<GeneralAttributeEnt> adapter;
    private ArrayList<GeneralAttributeEnt> collection;
    ArrayList<StudentEnt> studentEnts;

    public static GeneralAttributeMarksFragment newInstance() {
        Bundle args = new Bundle();

        GeneralAttributeMarksFragment fragment = new GeneralAttributeMarksFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ArrayListAdapter<GeneralAttributeEnt>(getDockActivity(), new GeneralAttItemBinder());

        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_general_attribute_marks, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mainframe.setVisibility(View.GONE);
        serviceHelper.enqueueCall(webService.getGeneralAttributes(), WebServiceConstants.getGeneralAttribute);
        spinner();

    }

    private void spinner() {


        serviceHelper.enqueueCall(webService.getstudent(), WebServiceConstants.getstudent);


        ArrayAdapter<CharSequence> year_adapter = ArrayAdapter.createFromResource(getDockActivity(), R.array.year, android.R.layout.simple_spinner_item);
        year_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        year.setAdapter(year_adapter);

        ArrayAdapter<CharSequence> exam_adapter = ArrayAdapter.createFromResource(getDockActivity(), R.array.exam, android.R.layout.simple_spinner_item);
        exam_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        term.setAdapter(exam_adapter);


    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.getGeneralAttribute:
                mainframe.setVisibility(View.VISIBLE);
                ArrayList<GeneralAttributeEnt> generalAtt = (ArrayList<GeneralAttributeEnt>) result;
                setData(generalAtt);
                break;

            case WebServiceConstants.getstudent:
                studentEnts = ((ArrayList<StudentEnt>) result);
                ArrayList<String> studentNames = new ArrayList<String>();
                for (StudentEnt ent : studentEnts
                        ) {
                    studentNames.add(ent.getName());
                }
                ArrayAdapter<String> student_adapter = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, studentNames);
                student_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                studentName.setAdapter(student_adapter);
                break;

            case WebServiceConstants.GeneralMarks:
                UIHelper.showShortToastInCenter(getDockActivity(),"Successfully Inserted");
                getDockActivity().replaceDockableFragment(HomeTeacherFragment.newInstance(),"HomeTeacherFragment");
                break;



        }
    }

    private void setData(ArrayList<GeneralAttributeEnt> generalAtt) {


        collection = new ArrayList<>();
      /*  collection.add(new ReportCardEnt("English", "100"));
        collection.add(new ReportCardEnt("Math", "100"));
        collection.add(new ReportCardEnt("Science", "100"));
        collection.add(new ReportCardEnt("Islamiyat", "100"));*/
        collection.addAll(generalAtt);

        adapter.clearList();
        lvGeneralAtt.setAdapter(adapter);
        adapter.addAll(collection);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("General Attribute Marks");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.submit)
    public void onViewClicked() {
     //   serviceHelper.enqueueCall(webService.addGeneralMarks(studentEnts.get(studentName.getSelectedItemPosition()).getId(), (String) year.getSelectedItem(), (String) term.getSelectedItem(), "24", "24", "24", "24", "24", "24"), WebServiceConstants.GeneralMarks);
        UIHelper.showShortToastInCenter(getDockActivity(), "Successfully Inserted");
        getDockActivity().replaceDockableFragment(HomeTeacherFragment.newInstance(),"HomeTeacherFragment");

    }
}
