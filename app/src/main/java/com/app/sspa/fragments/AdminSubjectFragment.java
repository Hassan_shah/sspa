package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.app.sspa.R;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.helpers.UIHelper;
import com.app.sspa.ui.views.AnyEditTextView;
import com.app.sspa.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AdminSubjectFragment extends BaseFragment {
    @BindView(R.id.subject_code)
    EditText subjectCode;
    @BindView(R.id.subject_name)
    EditText subjectName;
    @BindView(R.id.add_btn)
    Button addBtn;
    @BindView(R.id.cancel_btn)
    Button cancelBtn;
    Unbinder unbinder;

    public static AdminSubjectFragment newInstance() {
        return new AdminSubjectFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_add_subject, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        //addEmptyStringValidator((AnyEditTextView) subjectCode,(AnyEditTextView)subjectName);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Add Subject");
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag){
            case WebServiceConstants.addsubject:
                UIHelper.showShortToastInCenter(getDockActivity(), "Added Successfully");
                getDockActivity().popFragment();
                break;
        }
    }

    @OnClick({R.id.add_btn, R.id.cancel_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_btn:

                serviceHelper.enqueueCall(webService.addsubject(subjectCode.getText().toString(),subjectName.getText().toString()),WebServiceConstants.addsubject);
                break;
            case R.id.cancel_btn:
                getDockActivity().popFragment();
                break;
        }
    }
}