package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AdminViewRecordFragment extends BaseFragment {
    @BindView(R.id.btn_students)
    TextView btnStudents;
    @BindView(R.id.btn_teacher)
    TextView btnTeacher;
    @BindView(R.id.btn_view_announcement)
    TextView btnViewAnnouncement;
    Unbinder unbinder;

    public static AdminViewRecordFragment newInstance() {
        return new AdminViewRecordFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_admin_view_options, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("View Records");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_students, R.id.btn_teacher, R.id.btn_view_announcement})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_students:
                getDockActivity().replaceDockableFragment(AdminViewStudentFragment.newInstance(), "AdminViewStudentFragment");
                break;
            case R.id.btn_teacher:
                getDockActivity().replaceDockableFragment(AdminViewTeacherFragment.newInstance(), "AdminViewTeacherFragment");
                break;
            case R.id.btn_view_announcement:
                getDockActivity().replaceDockableFragment(AdminViewAnnouncementFragment.newInstance(), "AdminViewAnnouncementFragment");

                break;
        }
    }
}