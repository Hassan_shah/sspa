package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.GeneralAttributeEnt;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.helpers.UIHelper;
import com.app.sspa.ui.adapters.ArrayListAdapter;
import com.app.sspa.ui.viewbinders.abstracts.ShowAttributesBinder;
import com.app.sspa.ui.views.ExpandedListView;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AdminAttributeFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.txt_brandName)
    TextView txtBrandName;
    @BindView(R.id.mainframe)
    LinearLayout mainframe;
    @BindView(R.id.attr_name)
    EditText attrName;
    @BindView(R.id.cancel_btn)
    Button cancelBtn;
    @BindView(R.id.lv_generalAtt)
    ExpandedListView lvGeneralAtt;
    @BindView(R.id.add_btn)
    Button addBtn;

    private ArrayListAdapter<GeneralAttributeEnt> adapter;
    private ArrayList<GeneralAttributeEnt> collection;


    public static AdminAttributeFragment newInstance() {
        return new AdminAttributeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ArrayListAdapter<GeneralAttributeEnt>(getDockActivity(), new ShowAttributesBinder());

        if (getArguments() != null) {
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_add_attribute, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        //addEmptyStringValidator((AnyEditTextView) attrName);
        mainframe.setVisibility(View.GONE);
        serviceHelper.enqueueCall(webService.getGeneralAttributes(), WebServiceConstants.getGeneralAttribute);

    }


    private void setData(ArrayList<GeneralAttributeEnt> generalAtt) {
        collection = new ArrayList<>();
        collection.addAll(generalAtt);

        adapter.clearList();
        lvGeneralAtt.setAdapter(adapter);
        adapter.addAll(collection);
        adapter.notifyDataSetChanged();

    }


    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Add Attribute");
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.addattribute:
                UIHelper.showShortToastInCenter(getDockActivity(), "Added Successfully");
                getDockActivity().popFragment();
                break;

            case WebServiceConstants.getGeneralAttribute:
                mainframe.setVisibility(View.VISIBLE);
                ArrayList<GeneralAttributeEnt> generalAtt = (ArrayList<GeneralAttributeEnt>) result;
                setData(generalAtt);
                break;

        }
    }

    @OnClick({R.id.add_btn, R.id.cancel_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_btn:
                serviceHelper.enqueueCall(webService.addattribute(attrName.getText().toString()), WebServiceConstants.addattribute);
                break;
            case R.id.cancel_btn:
                getDockActivity().popFragment();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}