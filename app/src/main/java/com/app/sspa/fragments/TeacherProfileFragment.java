package com.app.sspa.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.TeacherProifleEnt;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Saeed Hyder on 12/1/2017.
 */
public class TeacherProfileFragment extends BaseFragment {

    @BindView(R.id.edtFirstName)
    TextView edtFirstName;
    @BindView(R.id.edtLastName)
    TextView edtLastName;
    @BindView(R.id.edtAge)
    TextView edtAge;
    @BindView(R.id.edtQualificationn)
    TextView edtQualificationn;
    @BindView(R.id.edtAddress)
    TextView edtAddress;
    @BindView(R.id.edtRegDate)
    TextView edtRegDate;
    @BindView(R.id.ll_profileItems)
    LinearLayout llProfileItems;
    @BindView(R.id.ll_profileDetail)
    LinearLayout llProfileDetail;
    Unbinder unbinder;

    public static TeacherProfileFragment newInstance() {
        return new TeacherProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teacher_profile, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        llProfileDetail.setVisibility(View.GONE);
        serviceHelper.enqueueCall(webService.getTeacherProfile(String.valueOf(prefHelper.getTeacherId())), WebServiceConstants.getannouncement);


    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.getannouncement:
                llProfileDetail.setVisibility(View.VISIBLE);
                setData((ArrayList<TeacherProifleEnt>) result);
                break;
        }
    }

    private void setData(ArrayList<TeacherProifleEnt> result) {

        edtFirstName.setText(result.get(0).getName());
        if (!result.get(0).getGender().equals(""))
            edtLastName.setText(result.get(0).getGender());
        edtAge.setText(result.get(0).getAge());
        edtQualificationn.setText(result.get(0).getQualification());
        edtAddress.setText(result.get(0).getAddress());
        edtRegDate.setText(result.get(0).getRegDate());

    }


    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Teacher Profile");
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
