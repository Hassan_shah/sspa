package com.app.sspa.fragments;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.classent;
import com.app.sspa.entities.sectionent;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Saeed Hyder on 12/12/2017.
 */
public class AddSectionFragment extends BaseFragment {

    @BindView(R.id.txt_brandName)
    TextView txtBrandName;
    @BindView(R.id.class_name)
    Spinner className;
    @BindView(R.id.section_name)
    EditText sectionName;
    @BindView(R.id.con_parent)
    TextInputLayout conParent;
    @BindView(R.id.add_btn)
    Button addBtn;
    @BindView(R.id.cancel_btn)
    Button cancelBtn;
    Unbinder unbinder;

    ArrayList<sectionent> sectionents;
    ArrayList<classent> classents;

    public static AddSectionFragment newInstance() {
        return new AddSectionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_add_section, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        //addEmptyStringValidator((AnyEditTextView) subjectCode,(AnyEditTextView)subjectName);
        serviceHelper.enqueueCall(webService.getclass(), WebServiceConstants.getclass);

    }



    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {

            case WebServiceConstants.getclass:
                ArrayList<String> classes = new ArrayList<String>();
                classents = (ArrayList<classent>) result;
                for (classent ent : classents
                        ) {
                    classes.add(ent.getClass_name());
                }
                ArrayAdapter<String> adapterclass = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, classes);
                adapterclass.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                className.setAdapter(adapterclass);
                break;

        }
    }




    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Add Section");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.add_btn, R.id.cancel_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_btn:
                getDockActivity().replaceDockableFragment(AdminHomeFragment.newInstance(),"AdminHomeFragment");
                break;
            case R.id.cancel_btn:
                getDockActivity().popFragment();
                break;
        }
    }
}
