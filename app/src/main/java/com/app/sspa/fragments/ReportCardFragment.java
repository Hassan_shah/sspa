package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.performanceEnt;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.ui.adapters.ArrayListAdapter;
import com.app.sspa.ui.viewbinders.abstracts.ReportCardViewBinder;
import com.app.sspa.ui.views.TitleBar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by saeedhyder on 12/12/2017.
 */
public class ReportCardFragment extends BaseFragment {
    @BindView(R.id.txt_studentName)
    TextView txtStudentName;
    @BindView(R.id.txt_class)
    TextView txtClass;
    @BindView(R.id.txt_section)
    TextView txtSection;
    @BindView(R.id.lv_reportcard)
    ListView lvReportcard;
    Unbinder unbinder;
    @BindView(R.id.txt_totalMarks)
    TextView txtTotalMarks;
    @BindView(R.id.txt_year)
    TextView txtYear;
    @BindView(R.id.txt_term)
    TextView txtTerm;

    private ArrayListAdapter<performanceEnt> adapter;
    private ArrayList<performanceEnt> collection;
    private static String PERFORMANCE = "performance";
    private static String YEAR = "year";
    private static String TERM = "term";
    //performanceEnt performance;
    ArrayList<performanceEnt> performance;

    public static ReportCardFragment newInstance() {
        Bundle args = new Bundle();
        ReportCardFragment fragment = new ReportCardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ReportCardFragment newInstance(ArrayList<performanceEnt> ent,String year,String Term) {
        Bundle args = new Bundle();
        args.putString(PERFORMANCE, new Gson().toJson(ent));
        args.putString(YEAR,year);
        args.putString(TERM,Term);
        ReportCardFragment fragment = new ReportCardFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new ArrayListAdapter<performanceEnt>(getDockActivity(), new ReportCardViewBinder());
        if (getArguments() != null) {
            //logs = gson.fromJson(br, new TypeToken<List<JsonLog>>(){}.getType());
            performance = new Gson().fromJson(getArguments().getString(PERFORMANCE), new TypeToken<List<performanceEnt>>() {
            }.getType());
            YEAR=getArguments().getString(YEAR);
            TERM=getArguments().getString(TERM);
            //performance = new Gson().fromJson(getArguments().getString(PERFORMANCE), performanceEnt.class);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reportcard, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setData();
    }

    private void setData() {

        int totalmarks = 0;
        int outof = 100 * performance.size();

        for (performanceEnt item : performance) {
            totalmarks = totalmarks + Integer.parseInt(item.getExam());
        }

        txtStudentName.setText(performance.get(0).getName());
        txtClass.setText("six");
        txtSection.setText("c");
        txtYear.setText(YEAR);
        txtTerm.setText(TERM);
        txtTotalMarks.setText(totalmarks + " / " + outof);


        collection = new ArrayList<>();
      /*  collection.add(new ReportCardEnt("English","100"));
        collection.add(new ReportCardEnt("Math","100"));
        collection.add(new ReportCardEnt("Science","100"));
        collection.add(new ReportCardEnt("Islamiyat","100"));*/

        collection.addAll(performance);

        adapter.clearList();
        lvReportcard.setAdapter(adapter);
        adapter.addAll(collection);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Report Card");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
