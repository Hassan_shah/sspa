package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.app.sspa.R;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Saeed Hyder on 12/13/2017.
 */
public class AddClassFragment extends BaseFragment {

    @BindView(R.id.class_name)
    EditText className;
    @BindView(R.id.add_btn)
    Button addBtn;
    @BindView(R.id.cancel_btn)
    Button cancelBtn;
    Unbinder unbinder;

    public static AddClassFragment newInstance() {
        return new AddClassFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_add_class, container, false);


        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Add Class");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.add_btn, R.id.cancel_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_btn:
                getDockActivity().replaceDockableFragment(AdminHomeFragment.newInstance(),"AdminHomeFragment");
                break;
            case R.id.cancel_btn:
                getDockActivity().popFragment();
                break;
        }
    }
}
