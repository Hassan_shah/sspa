package com.app.sspa.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.helpers.DialogHelper;
import com.app.sspa.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SideMenuFragment extends BaseFragment {

    @BindView(R.id.home)
    TextView home;
    @BindView(R.id.Profile)
    TextView Profile;
    @BindView(R.id.Notification)
    TextView Notification;
    @BindView(R.id.ViewMarks)
    TextView ViewMarks;
    @BindView(R.id.Logout)
    TextView Logout;
    Unbinder unbinder;

    public static SideMenuFragment newInstance() {
        return new SideMenuFragment();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sidemenu, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }


    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideTitleBar();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @OnClick({R.id.home, R.id.Profile, R.id.Notification, R.id.ViewMarks, R.id.Logout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.home:
                getDockActivity().replaceDockableFragment(HomeStudentFragment.newInstance(),"HomeStudentFragment");
                break;
            case R.id.Profile:
                getDockActivity().replaceDockableFragment(StudentProfileFragment.newInstance(),"StudentProfileFragment");
                break;
            case R.id.Notification:
                getDockActivity().replaceDockableFragment(StudentNotificationFragment.newInstance(),"StudentNotificationFragment");
                break;
            case R.id.ViewMarks:
                getDockActivity().replaceDockableFragment(StudentViewMarksFragment.newInstance(),"StudentViewMarksFragment");
            break;
            case R.id.Logout:
                final DialogHelper logout = new DialogHelper(getDockActivity());
                logout.logoutDialoge(R.layout.logout_dialog, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        prefHelper.setLoginStatus(false);
                        prefHelper.setIsStudent(false);
                        getDockActivity().replaceDockableFragment(LoginFragment.newInstance(), "LoginFragment");
                        logout.hideDialog();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        logout.hideDialog();
                    }
                });
                logout.showDialog();
                break;
        }
    }
}
