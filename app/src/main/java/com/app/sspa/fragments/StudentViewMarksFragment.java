package com.app.sspa.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.app.sspa.R;
import com.app.sspa.entities.StudentEnt;
import com.app.sspa.entities.performanceEnt;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.helpers.UIHelper;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Saeed Hyder on 12/6/2017.
 */
public class StudentViewMarksFragment extends BaseFragment {

    @BindView(R.id.year)
    Spinner year;
    @BindView(R.id.subject)
    Spinner subject;
    @BindView(R.id.send)
    Button send;
    Unbinder unbinder;
    ArrayList<StudentEnt> studentEnts;

    public static StudentViewMarksFragment newInstance() {
        return new StudentViewMarksFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_student_viewmarks, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinners();
    }

    private void spinners() {
        serviceHelper.enqueueCall(webService.getstudent(), WebServiceConstants.getstudent);


        ArrayAdapter<CharSequence> year_adapter = ArrayAdapter.createFromResource(getDockActivity(), R.array.year, android.R.layout.simple_spinner_item);
        year_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        year.setAdapter(year_adapter);

        ArrayAdapter<CharSequence> exam_adapter = ArrayAdapter.createFromResource(getDockActivity(), R.array.exam, android.R.layout.simple_spinner_item);
        exam_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subject.setAdapter(exam_adapter);


    }


    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.getperformance:
                ArrayList<performanceEnt> ent = (ArrayList<performanceEnt>) result;
                if (ent != null && ent.size() > 0)
                    getDockActivity().replaceDockableFragment(ReportCardFragment.newInstance(ent, (String) year.getSelectedItem(), (String) subject.getSelectedItem()), "ReportCardFragment");
                else
                    UIHelper.showShortToastInCenter(getDockActivity(), "No Data To Show");

                break;
        }
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("View Marks");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.send)
    public void onClick() {
        serviceHelper.enqueueCall(webService.getperformance(String.valueOf(prefHelper.getStudentId()),(String) year.getSelectedItem(), (String) subject.getSelectedItem()), WebServiceConstants.getperformance);

    }
}
