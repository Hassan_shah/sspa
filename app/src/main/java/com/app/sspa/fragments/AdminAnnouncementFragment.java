package com.app.sspa.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.helpers.UIHelper;
import com.app.sspa.ui.views.AnyEditTextView;
import com.app.sspa.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdminAnnouncementFragment extends BaseFragment {


    @BindView(R.id.txt_brandName)
    TextView txtBrandName;
    @BindView(R.id.announcement_code)
    EditText announcementCode;
    @BindView(R.id.announcement_title)
    EditText announcementTitle;
    @BindView(R.id.announcement_decp)
    EditText announcementDecp;
    @BindView(R.id.add_btn)
    Button addBtn;
    @BindView(R.id.cancel_btn)
    Button cancelBtn;

    public static AdminAnnouncementFragment newInstance() {
        return new AdminAnnouncementFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_add_anouncement, container, false);
         ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Add Announcement");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        //addEmptyStringValidator((AnyEditTextView) announcementCode,(AnyEditTextView)announcementTitle,(AnyEditTextView)announcementDecp);
    }
    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag){
            case WebServiceConstants.addannouncement:
                UIHelper.showShortToastInCenter(getDockActivity(), "Added Successfully");
                getDockActivity().popFragment();
                break;
        }
    }

    @OnClick({R.id.add_btn, R.id.cancel_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_btn:
                serviceHelper.enqueueCall(webService.addannouncement(announcementCode.getText().toString(),announcementTitle.getText().toString()
                        ,announcementDecp.getText().toString()), WebServiceConstants.addannouncement);
                break;
            case R.id.cancel_btn:
                getDockActivity().popFragment();
                break;
        }
    }


}