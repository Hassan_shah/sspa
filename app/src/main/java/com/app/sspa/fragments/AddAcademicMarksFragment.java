package com.app.sspa.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.app.sspa.R;
import com.app.sspa.entities.GeneralAttributeEnt;
import com.app.sspa.entities.StudentEnt;
import com.app.sspa.entities.classent;
import com.app.sspa.entities.sectionent;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.helpers.UIHelper;
import com.app.sspa.ui.adapters.ArrayListAdapter;
import com.app.sspa.ui.viewbinders.abstracts.GeneralAttItemBinder;
import com.app.sspa.ui.views.ExpandedListView;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Saeed Hyder on 12/1/2017.
 */
public class AddAcademicMarksFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.section)
    Spinner section;
    @BindView(R.id.studentName)
    Spinner studentName;
    @BindView(R.id.year)
    Spinner year;
    @BindView(R.id.term)
    Spinner term;
    @BindView(R.id.classspinner)
    Spinner classspinner;
    @BindView(R.id.lv_addmarks)
    ExpandedListView lvAddmarks;
    @BindView(R.id.English)
    EditText English;
    @BindView(R.id.Science)
    EditText Science;
    @BindView(R.id.Mathmatics)
    EditText Mathmatics;
    @BindView(R.id.Urdu)
    EditText Urdu;
    @BindView(R.id.Islamiyat)
    EditText Islamiyat;
    @BindView(R.id.SocialStudies)
    EditText SocialStudies;
    @BindView(R.id.Computer)
    EditText Computer;

    private ArrayListAdapter<GeneralAttributeEnt> adapter;
    private ArrayList<GeneralAttributeEnt> collection;

    ArrayList<sectionent> sectionents;
    ArrayList<classent> classents;
    ArrayList<StudentEnt> studentEnts;

    public static AddAcademicMarksFragment newInstance() {
        return new AddAcademicMarksFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ArrayListAdapter<GeneralAttributeEnt>(getDockActivity(), new GeneralAttItemBinder());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_addacademicmarks, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinner();
        setData();
    }

    private void setData() {


        collection = new ArrayList<>();
        collection.add(new GeneralAttributeEnt(100, "English"));
        collection.add(new GeneralAttributeEnt(100, "Science"));
        collection.add(new GeneralAttributeEnt(100, "Mathmatics"));
        collection.add(new GeneralAttributeEnt(100, "Urdu"));
        collection.add(new GeneralAttributeEnt(100, "Islamiyat"));
        collection.add(new GeneralAttributeEnt(100, "Social Studies"));
        collection.add(new GeneralAttributeEnt(100, "Computer"));

        adapter.clearList();
        lvAddmarks.setAdapter(adapter);
        adapter.addAll(collection);
        adapter.notifyDataSetChanged();

    }


    private void spinner() {

        serviceHelper.enqueueCall(webService.getclass(), WebServiceConstants.getclass);
      /*  List<String> classlist = new ArrayList<String>();
        classlist.add("class 1");
        classlist.add("class 2");
        classlist.add("class 3");
        classlist.add("class 4");

        ArrayAdapter<String> classAdapter = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, classlist);
        classAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        classspinner.setAdapter(classAdapter);*/

        serviceHelper.enqueueCall(webService.getsections(), WebServiceConstants.getsections);

      /*  List<String> sectionlist = new ArrayList<String>();
        sectionlist.add("Section A");
        sectionlist.add("Section B");
        sectionlist.add("Section C");

        ArrayAdapter<String> sectionAdapter1 = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, sectionlist);
        sectionAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        section.setAdapter(sectionAdapter1);*/

        serviceHelper.enqueueCall(webService.getstudent(), WebServiceConstants.getstudent);

      /*  List<String> studentlist = new ArrayList<String>();
        studentlist.add("Studnet 1");
        studentlist.add("Studnet 2");
        studentlist.add("Studnet 3");

        ArrayAdapter<String> studentAdapter1 = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, studentlist);
        studentAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        studentName.setAdapter(studentAdapter1);
*/
        ArrayAdapter<CharSequence> year_adapter = ArrayAdapter.createFromResource(getDockActivity(), R.array.year, android.R.layout.simple_spinner_item);
        year_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        year.setAdapter(year_adapter);

        ArrayAdapter<CharSequence> exam_adapter = ArrayAdapter.createFromResource(getDockActivity(), R.array.exam, android.R.layout.simple_spinner_item);
        exam_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        term.setAdapter(exam_adapter);


    }

    private boolean isvalidated() {
        if (English.getText().toString().equals("")) {
            English.setError(getString(R.string.enter_filed));
            if (English.requestFocus()) {
                getDockActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return false;
        } else if (Science.getText().toString().equals("")) {
            Science.setError(getString(R.string.enter_filed));
            if (Science.requestFocus()) {
                getDockActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return false;
        } else if (Mathmatics.getText().toString().equals("")) {
            Mathmatics.setError(getString(R.string.enter_filed));
            if (Mathmatics.requestFocus()) {
                getDockActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return false;
        } else if (Urdu.getText().toString().equals("")) {
            Urdu.setError(getString(R.string.enter_filed));
            if (Urdu.requestFocus()) {
                getDockActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return false;
        } else if (Islamiyat.getText().toString().equals("")) {
            Islamiyat.setError(getString(R.string.enter_filed));
            if (Islamiyat.requestFocus()) {
                getDockActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return false;
        } else if (SocialStudies.getText().toString().equals("")) {
            SocialStudies.setError(getString(R.string.enter_filed));
            if (SocialStudies.requestFocus()) {
                getDockActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return false;
        } else if (Computer.getText().toString().equals("")) {
            Computer.setError(getString(R.string.enter_filed));
            if (Computer.requestFocus()) {
                getDockActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.getsections:
                ArrayList<String> sections = new ArrayList<String>();
                sectionents = (ArrayList<sectionent>) result;
                for (sectionent ent : sectionents
                        ) {
                    sections.add(ent.getSection_name());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, sections);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                section.setAdapter(adapter);
                break;
            case WebServiceConstants.getclass:
                ArrayList<String> classes = new ArrayList<String>();
                classents = (ArrayList<classent>) result;
                for (classent ent : classents
                        ) {
                    classes.add(ent.getClass_name());
                }
                ArrayAdapter<String> adapterclass = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, classes);
                adapterclass.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                classspinner.setAdapter(adapterclass);
                break;

            case WebServiceConstants.getstudent:
                studentEnts = ((ArrayList<StudentEnt>) result);
                ArrayList<String> studentNames = new ArrayList<String>();
                for (StudentEnt ent : studentEnts
                        ) {
                    studentNames.add(ent.getName());
                }
                ArrayAdapter<String> student_adapter = new ArrayAdapter<String>(getDockActivity(), android.R.layout.simple_spinner_item, studentNames);
                student_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                studentName.setAdapter(student_adapter);
                break;
            case WebServiceConstants.academicmarks:
                UIHelper.showShortToastInCenter(getDockActivity(), "Successfully Inserted");
                getDockActivity().replaceDockableFragment(HomeTeacherFragment.newInstance(), "HomeTeacherFragment");
                break;

        }
    }


    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Academic Marks");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.submit)
    public void onClick() {
        if (isvalidated()){

            UIHelper.showShortToastInCenter(getDockActivity(), "Successfully Inserted");
            getDockActivity().replaceDockableFragment(HomeTeacherFragment.newInstance(), "HomeTeacherFragment");
//           serviceHelper.enqueueCall(webService.addAcademicMarks(studentEnts.get(studentName.getSelectedItemPosition()).getId(),(String) year.getSelectedItem(),(String)term.getSelectedItem(),"24","24","24","24","24","24","24","eng","24","24","24"), WebServiceConstants.academicmarks);
    }
    }
}
