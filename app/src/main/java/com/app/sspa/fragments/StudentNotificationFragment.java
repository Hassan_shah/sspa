package com.app.sspa.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.announcementEnt;
import com.app.sspa.fragments.abstracts.BaseFragment;
import com.app.sspa.global.WebServiceConstants;
import com.app.sspa.ui.adapters.ArrayListAdapter;
import com.app.sspa.ui.viewbinders.abstracts.AnnouncementBinder;
import com.app.sspa.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Saeed Hyder on 12/6/2017.
 */
public class StudentNotificationFragment extends BaseFragment {

    @BindView(R.id.lv_viewNotification)
    ListView lvViewNotification;
    Unbinder unbinder;

    ArrayList<String> notifications = new ArrayList<>();
    ArrayAdapter<String> listAdapter;
    @BindView(R.id.txt_nodata)
    TextView txtNodata;

    private ArrayListAdapter<announcementEnt> adapter;
    private ArrayList<announcementEnt> collections;

    public static StudentNotificationFragment newInstance() {
        return new StudentNotificationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ArrayListAdapter<announcementEnt>(getDockActivity(), new AnnouncementBinder());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_student_notification, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*notifications=new ArrayList<>();
        notifications.add("notification 1");
        notifications.add("notification 2");
        notifications.add("notification 3");
        notifications.add("notification 4");

        listAdapter = new ArrayAdapter<String>(getDockActivity(), R.layout.simplerow, notifications);
        lvViewNotification.setAdapter(listAdapter);*/

        serviceHelper.enqueueCall(webService.getannouncement(), WebServiceConstants.getannouncement);


    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.getannouncement:
                setData((ArrayList<announcementEnt>) result);
                break;
        }
    }

    private void setData(ArrayList<announcementEnt> result) {
        collections = new ArrayList<>();
        collections.addAll(result);

        if (collections.size() <= 0) {
            lvViewNotification.setVisibility(View.GONE);
            txtNodata.setVisibility(View.VISIBLE);
        } else {
            lvViewNotification.setVisibility(View.VISIBLE);
            txtNodata.setVisibility(View.GONE);
        }
        adapter.clearList();
        adapter.addAll(collections);
        lvViewNotification.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading("Student Notification");
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
