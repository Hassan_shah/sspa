package com.app.sspa.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;


public class BasePreferenceHelper extends PreferenceHelper {

    private Context context;

    protected static final String KEY_LOGIN_STATUS = "islogin";
    protected static final String KEY_ISADMIN = "isadmin";
    protected static final String KEY_IS_STUDENT = "isstudent";
    protected static final String KEY_IS_TEACHER = "isteacher";
    protected static final String KEY_STUDENTID = "studentid";
    protected static final String KEY_TEACHERID = "teacherid";
    protected static final String KEY_ADMINID = "adminId";
    protected static final String KEY_STUDENT_NAME = "studentname";

    private static final String FILENAME = "preferences";


    public BasePreferenceHelper(Context c) {
        this.context = c;
    }

    public SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(FILENAME, Activity.MODE_PRIVATE);
    }

    public void setLoginStatus( boolean isLogin ) {
        putBooleanPreference( context, FILENAME, KEY_LOGIN_STATUS, isLogin );
    }

    public boolean isLogin() {
        return getBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS);
    }

    public boolean isAdmin() {
        return getBooleanPreference(context, FILENAME, KEY_ISADMIN);
    }

    public void setIsAdmin( boolean isAdmin ) {
        putBooleanPreference( context, FILENAME,KEY_ISADMIN , isAdmin );
    }

    public boolean isStudent() {
        return getBooleanPreference(context, FILENAME, KEY_IS_STUDENT);
    }

    public void setIsStudent( boolean isStudent ) {
        putBooleanPreference( context, FILENAME,KEY_IS_STUDENT , isStudent );
    }

    public boolean isTeacher() {
        return getBooleanPreference(context, FILENAME, KEY_IS_TEACHER);
    }

    public void setIsTeacher( boolean isTeacher ) {
        putBooleanPreference( context, FILENAME,KEY_IS_TEACHER , isTeacher );
    }

    public int getStudentId() {
        return getIntegerPreference(context, FILENAME, KEY_STUDENTID);
    }

    public void setStudentId( int isStudent ) {
        putIntegerPreference( context, FILENAME,KEY_STUDENTID , isStudent );
    }

    public int getTeacherId() {
        return getIntegerPreference(context, FILENAME, KEY_TEACHERID);
    }

    public void setTeacherId( int isTeacher ) {
        putIntegerPreference( context, FILENAME,KEY_TEACHERID , isTeacher );
    }

    public int getAdminId() {
        return getIntegerPreference(context, FILENAME, KEY_ADMINID);
    }

    public void setAdminId( int isAdmin ) {
        putIntegerPreference( context, FILENAME,KEY_ADMINID , isAdmin );
    }

    public String getStudentName() {
        return getStringPreference(context, FILENAME, KEY_STUDENT_NAME);
    }

    public void setStudentName( String studentName ) {
        putStringPreference( context, FILENAME,KEY_STUDENT_NAME , studentName );
    }


}
