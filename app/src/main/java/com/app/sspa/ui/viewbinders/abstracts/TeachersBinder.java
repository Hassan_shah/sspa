package com.app.sspa.ui.viewbinders.abstracts;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.teacherEnt;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hasssan on 12/3/2017.
 */

public class TeachersBinder extends ViewBinder<teacherEnt> {
    public TeachersBinder() {
        super(R.layout.row_item_admin_teacher);
    }

    @Override
    public BaseViewHolder createViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void bindView(teacherEnt entity, int position, int grpPosition, View view, Activity activity) {
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.name.setText("Name : " + entity.getName());
        holder.age.setText("Age : " + entity.getAge());
        holder.address.setText("Address : " + entity.getAddress());
        holder.gender.setText("Gender : " + entity.getGender());
        holder.qualification.setText("Qualification : " + entity.getQualification());
    }


    static class ViewHolder extends BaseViewHolder{
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.gender)
        TextView gender;
        @BindView(R.id.age)
        TextView age;
        @BindView(R.id.qualification)
        TextView qualification;
        @BindView(R.id.address)
        TextView address;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
