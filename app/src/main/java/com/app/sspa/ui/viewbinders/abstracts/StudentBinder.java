package com.app.sspa.ui.viewbinders.abstracts;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.StudentEnt;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hasssan on 12/3/2017.
 */

public class StudentBinder extends ViewBinder<StudentEnt> {
    public StudentBinder() {
        super(R.layout.row_item_admin_student_record);
    }

    @Override
    public BaseViewHolder createViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void bindView(StudentEnt entity, int position, int grpPosition, View view, Activity activity) {
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.name.setText("Name : " + entity.getName());
        holder.parentName.setText("Parent Name : " + entity.getParent());
        holder.address.setText("Address : " + entity.getAddress());
        holder.gender.setText("Gender : " + entity.getGender());
        holder.className.setText("Class : " + entity.getClass_name());
        holder.section.setText("Section : " + entity.getSection_name());
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.parent_name)
        TextView parentName;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.gender)
        TextView gender;
        @BindView(R.id.class_name)
        TextView className;
        @BindView(R.id.section)
        TextView section;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
