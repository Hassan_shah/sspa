package com.app.sspa.ui.viewbinders.abstracts;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.announcementEnt;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hasssan on 12/3/2017.
 */

public class AnnouncementBinder extends ViewBinder<announcementEnt> {
    public AnnouncementBinder() {
        super(R.layout.row_item_admin_announcement);
    }

    @Override
    public BaseViewHolder createViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void bindView(announcementEnt entity, int position, int grpPosition, View view, Activity activity) {
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.announcementCode.setText("Announcement Code : " + entity.getAnnouncement_id());
        holder.title.setText("Announcement Title : " + entity.getTitle());
        holder.description.setText("Announcement Description : " + entity.getDescription());
        holder.time.setText("Announcement Time : " + entity.getDate());
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R.id.announcement_code)
        TextView announcementCode;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.time)
        TextView time;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
