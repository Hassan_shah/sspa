package com.app.sspa.ui.viewbinders.abstracts;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.GeneralAttributeEnt;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Saeed Hyder on 12/13/2017.
 */
public class ShowAttributesBinder extends ViewBinder<GeneralAttributeEnt> {

    public ShowAttributesBinder() {
        super(R.layout.row_item_atttributes);

    }

    @Override
    public BaseViewHolder createViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void bindView(GeneralAttributeEnt entity, int position, int grpPosition, View view, Activity activity) {

        final ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.subject.setText(entity.getAttribute_name());

    }


    static class ViewHolder extends BaseViewHolder{
        @BindView(R.id.subject)
        TextView subject;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}