package com.app.sspa.ui.views;

import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.app.sspa.R;


public class Util {
	
	public static Map<String, Typeface> typefaceCache = new HashMap<String, Typeface>();
	
	@SuppressLint("StringFormatInvalid")
	public static void setTypeface(AttributeSet attrs, TextView textView ) {
		Context context = textView.getContext();
		
		TypedArray values = context.obtainStyledAttributes( attrs,
				R.styleable.AnyTextView );
		String typefaceName = values
				.getString( R.styleable.AnyTextView_typeface );
		
		if ( typefaceCache.containsKey( typefaceName ) ) {
			textView.setTypeface( typefaceCache.get( typefaceName ) );
		} else {
			Typeface typeface;
			try {
				typeface = Typeface.createFromAsset( textView.getContext()
						.getAssets(),
						context.getString( R.string.assets_fonts_folder )
								+ typefaceName );
			} catch ( Exception e ) {
				Log.v( context.getString( R.string.app ), String.format(context.getResources().getString( R.string.typeface_not_found ),
						typefaceName ) );
				return;
			}
			
			typefaceCache.put( typefaceName, typeface );
			textView.setTypeface( typeface );
		}
		values.recycle();
	}
	
	@SuppressLint("StringFormatInvalid")
	public static void setTypefaceUpdated(AttributeSet attrs, TextView textView ) {
		Context context = textView.getContext();
		
		TypedArray values = context.obtainStyledAttributes( attrs,
				R.styleable.AnyTextView );
		String typefaceName = values
				.getString( R.styleable.AnyTextView_typeface );
		
		if ( typefaceCache.containsKey( typefaceName ) ) {
			textView.setTypeface( typefaceCache.get( typefaceName ) );
		} else {
			Typeface typeface;
			try {
				typeface = Typeface.createFromAsset( textView.getContext()
						.getAssets(),
						context.getString( R.string.assets_fonts_folder )
								+ typefaceName );
			} catch ( Exception e ) {
				Log.v( context.getString( R.string.app ), String.format(
						context.getString( R.string.typeface_not_found ),
						typefaceName ) );
				return;
			}
			
			typefaceCache.put( typefaceName, typeface );
			textView.setTypeface( typeface );
		}
		
		values.recycle();
	}
}