package com.app.sspa.ui.viewbinders.abstracts;

import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.GeneralAttributeEnt;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by saeedhyder on 12/12/2017.
 */

public class GeneralAttItemBinder extends ViewBinder<GeneralAttributeEnt> {

    public GeneralAttItemBinder() {
        super(R.layout.row_item_general_att);

    }

    @Override
    public BaseViewHolder createViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void bindView(GeneralAttributeEnt entity, int position, int grpPosition, View view, Activity activity) {

        final ViewHolder viewHolder= (ViewHolder) view.getTag();
        viewHolder.subject.setText(entity.getAttribute_name());

    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R.id.subject)
        TextView subject;
        @BindView(R.id.marks)
        EditText marks;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
