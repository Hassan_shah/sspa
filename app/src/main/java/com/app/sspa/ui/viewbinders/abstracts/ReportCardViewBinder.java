package com.app.sspa.ui.viewbinders.abstracts;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.app.sspa.R;
import com.app.sspa.entities.performanceEnt;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by saeedhyder on 12/12/2017.
 */

public class ReportCardViewBinder extends ViewBinder<performanceEnt> {

    public ReportCardViewBinder() {
        super(R.layout.row_item_reportcard);

    }

    @Override
    public BaseViewHolder createViewHolder(View view) {
        return new ViewHolder(view);
    }


    @Override
    public void bindView(performanceEnt entity, int position, int grpPosition, View view, Activity activity) {

        final ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.subject.setText(entity.getSubject_name()+"");
        viewHolder.marks.setText(entity.getExam()+"");
        viewHolder.Totalmarks.setText("100");

    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R.id.subject)
        TextView subject;
        @BindView(R.id.marks)
        TextView marks;
        @BindView(R.id.Totalmarks)
        TextView Totalmarks;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
