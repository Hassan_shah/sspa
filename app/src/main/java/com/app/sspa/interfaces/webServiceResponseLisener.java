package com.app.sspa.interfaces;

/**
 * Created by Hasssan on 10/20/2017.
 */

public interface webServiceResponseLisener<T> {
    public void ResponseSuccess(T result, String Tag);

    public void ResponseFailure(String tag);
}
