package com.app.sspa.entities;

/**
 * Created by Saeed Hyder on 12/13/2017.
 */
public class GeneralAttributeEnt {


    int id;
    String Attribute_name;

    public GeneralAttributeEnt(int id, String attribute_name) {
        this.id = id;
        Attribute_name = attribute_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAttribute_name() {
        return Attribute_name;
    }

    public void setAttribute_name(String attribute_name) {
        Attribute_name = attribute_name;
    }
}
