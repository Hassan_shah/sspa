package com.app.sspa.entities;

public class ResponseWrapper<T> {

    private String responseMessage;
    private Integer response;
    private T result;

    public String getMessage() {
        return responseMessage;
    }

    public void setMessage(String message) {
        responseMessage = message;
    }

    public Integer getResponse() {
        return response;
    }

    public void setResponse(Integer response) {
        this.response = response;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
