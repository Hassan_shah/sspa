package com.app.sspa.entities;

/**
 * Created by saeedhyder on 12/12/2017.
 */

public class ReportCardEnt {

    String subject;
    String marks;

    public ReportCardEnt(String subject, String marks) {
        this.subject = subject;
        this.marks = marks;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }
}
