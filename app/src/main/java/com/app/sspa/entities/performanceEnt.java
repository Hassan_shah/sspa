package com.app.sspa.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hasssan on 12/11/2017.
 */

public class performanceEnt {

    @SerializedName("name")
    private String name;
    @SerializedName("subject_name")
    private String subject_name;
    @SerializedName("test")
    private String test;
    @SerializedName("exam")
    private String exam;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }
}
